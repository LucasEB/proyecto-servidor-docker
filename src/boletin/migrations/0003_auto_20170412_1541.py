# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-12 13:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boletin', '0002_contacto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contacto',
            name='mensaje',
            field=models.TextField(max_length=300),
        ),
    ]
