from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Registrado(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField()
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.email


class Contacto(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)  # blank=form; null=database
    email = models.EmailField()
    mensaje = models.TextField(max_length=300, blank=False, null=False)

    def __str__(self):
        return self.email


class Pelicula(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    ruta = models.CharField(max_length=100, blank=True, null=True)
    caratula = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre


class Serie(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    ruta = models.CharField(max_length=100, blank=True, null=True)
    caratula = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre


class Documental(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    ruta = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre
