from django import forms

from .models import Registrado
from .models import Contacto

class RegModelForm(forms.ModelForm):
    class Meta:
        model = Registrado
        fields = ["nombre", "email"]

    def clean_email(self):
        """
        print (self.cleaned_data)
        return "email@email.com"
        """
        email = self.cleaned_data.get("email")
        email_base, proveedor = email.split("@")
        dominio, extension = proveedor.split(".")
        if not extension == "edu":
            raise forms.ValidationError("Por favor utiliza un email con la extensión .edu")
        return email

    def clean_nombre(self):
        # Pendiente de validaciones para el nombre
        nombre = self.cleaned_data.get("nombre")
        return nombre

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = ["nombre", "email", "mensaje"]

    def clean_email(self):
        email = self.cleaned_data.get("email")
        # email_base, proveedor = email.split("@")
        # dominio, extension = proveedor.split(".")
        # if not extension == "edu":
        #     raise forms.ValidationError("Por favor utiliza un email con la extensión .edu")
        return email

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if not nombre:
            nombre = "SinNombre"
        return nombre
