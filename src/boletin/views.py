from os import listdir, path, walk
from os.path import isfile, join
import tempfile
import zipfile
import mimetypes

from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, FileResponse
from django.urls import reverse
from wsgiref.util import FileWrapper

from .forms import RegModelForm, ContactForm
from .models import Registrado, Contacto, Pelicula, Serie, Documental


# Create your views here.
def inicio(request):
    titulo = "Bienvenido!"
    user = "No User"
    if request.user.is_authenticated():
        titulo = "Bienvenido %s" %(request.user)
        user = request.user
    form = RegModelForm(request.POST or None)

    context = {
        "wb_titulo": titulo,
        "wb_form": form,
        "wb_user": user,
    }

    if form.is_valid():
        instance = form.save(commit=False)  # Hacemos esto por si queremos modificar el resultado antes de guardarlo en la bd
        nombre = form.cleaned_data.get("nombre")
        email = form.cleaned_data.get("email")

        context = {
            "wb_titulo": "Gracias %s!" %(nombre)
        }

        if not instance.nombre:  # Modificación de la instancia antes de guardarla en la bd
            instance.nombre = "SinNombre"
            context = {
                "wb_titulo": "Gracias %s!" %(email)
            }

        instance.save()

        # print(instance.nombre)
        # print(instance.email)
        print(instance)
        print(instance.timestamp)
        # form_data = form.cleaned_data
        # abc = form_data.get("email")
        # abc2 = form_data.get("nombre")
        # obj = Registrado.objects.create(email=abc, nombre=abc2)

    return render(request, "inicio.html", context)


def multimedia(request):
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    else:
        titulo = "Bienvenido %s" %(request.user)
        user = request.user

    return render(request, "multimedia.html", {})


def galeria(request, titulo="./static/media/fotos"):
    # La variable titulo contiene la ruta a escanear que por defecto es
    # ./static/media/fotos

    # Comprobamos si el usuario está registrado, si no lo redirigimos a inicio
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    else:
        title = "Bienvenido %s" %(request.user)

    # for elementos in listdir(titulo):  # ../media/fotos
    #     elemento_cut = elementos.split('.')
    #     if len(elemento_cut) == 2:
    #         files.append(elementos)
    #     elif len(elemento_cut) == 1:
    #         folders.append(elementos)

    # Modificamos la variable titulo, la cual contiene la ruta que vamos a
    # escanear en busca de imágenes y guardamos la ultima parte de la ruta
    # que nos dará el nombre de la carpeta en la que estamos.
    pactual = titulo.split('/')[-1]

    # Creamos un bucle que guarde en 3 variables distintas la ruta a escanear,
    # las carpetas contenidas y tambien las fotos que haya en dicha ruta.
    for ruta, folders, files in walk(titulo):
        break

    # Modificamos la ruta a escanear para mostrar correctamente las imágenes.
    titulo = titulo.replace('static/', '')

    # Establecemos las variables que randerizaremos en el html
    context = {
        'files': files,
        'folders': folders,
        'rutaf': ruta,
        'rutai': titulo,
        'pactual': pactual
    }

    return render(request, "galeria.html", context)


def peliculas(request):
    # Comprobamos si el usuario está registrado, si no lo redirigimos a inicio
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    else:
        titulo = "Bienvenido %s" %(request.user)

    # Creamos la variable archivo que guardará el nombre, ruta y carátula de
    # cada película que tengamos guardada en la base de datos.
    archivos = Pelicula.objects.values_list('nombre', 'ruta', 'caratula').order_by('nombre')

    # Contamos el número de resultados obtenidos por la consulta y dividimos/2
    total = len(archivos)
    mitad = int(total / 2)

    if total == 1:
        archivos1 = archivos
    else:
        # Comprobamos si el total de películas es multiplo de 2 y según esto
        # dividimos las películas en dos variables, la primera formará una
        # columna en el lado izquierdo de la página y la segunda en el derecho
        if total % 2 != 0:
            mitad = mitad + 1
            archivos1 = archivos[0:mitad]
            archivos2 = archivos[mitad:]
        else:
            archivos1 = archivos[0:mitad]
            archivos2 = archivos[mitad:]

    # Pasamos las siguientes variables que se randerizarán en la página
    context = {
        'archivos1': archivos1,
        'archivos2': archivos2,
        'titulo': titulo
    }

    return render(request, "peliculas.html", context)


def sinc_peliculas(request):
    # Pelicula.objects.all().delete()

    # Primero guardamos en una variable los nombres de los archivos que
    # tengamos en la carpeta caratulas, que tendrán el mismo nombre que
    # la película (ej.: "pelicula.avi.jpg")
    caratulas = listdir('./static/images/caratulas/peliculas')

    # Mediante la función walk recorremos la carpeta donde tenemos las
    # películas y guardamos la ruta y nombre de archivo.
    for path, folders, elementos in walk("./static/media/videos"):

        # Comprobamos primero que en la ruta actual haya algún video,
        # por si la carpeta está vacia
        if len(elementos) > 0:

            # Si existen videos recorremos cada valor mediante un bucle for
            for filename in elementos:

                # Reemplazamos la doble contrabarra de la ruta por un slash
                path_rep = path.replace('\\', '/')

                # Modificamos la ruta, añadiendole el nombre de archivo
                ruta = path_rep + '/' + filename

                # Si la película existe en la base de datos guardamos la
                # variable con los valores de dicha instancia
                try:
                    pelicula = Pelicula.objects.get(nombre=filename)

                # Si no es así, creamos una nueva instancia
                except:
                    pelicula = Pelicula()
                    pelicula.nombre = filename

                # Si hay alguna carátula con el nombre de la película
                # guardamos la ruta de la caratula
                if filename in str(caratulas):
                    pelicula.caratula = 'images/caratulas/peliculas/' + filename + '.jpg'
                pelicula.ruta = ruta

                # Guardamos la instancia en la bbdd
                pelicula.save()
                pelicula = Pelicula()

    return HttpResponseRedirect(reverse('peliculas'))


def descargarp(request, ruta):
    # Comprobamos si el usuario está registrado, si no lo redirigimos a inicio
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))

    # nos traemos el objeto cuya ruta sea la misma que la variable ruta
    # corregida, con ./ añadido antes de la ruta suministrada
    pelicula = Pelicula.objects.get(ruta='./' + ruta)

        # nombre = pelicula.nombre.replace(' ', '_')

    # Abrimos la pelicula y la guardamos en una variable
    fichero = open(ruta, 'rb')

    # Indicamos que la respuesta será de tipo streaming
    response = FileResponse(fichero)

        # wrapper = FileWrapper(fichero)

    # Guardamos el tipo de archivo en una variable gracias a la función
    # guess_type que importamos previamente mediante import mimetypes
    content_type = mimetypes.guess_type(ruta)[0]

        # response = HttpResponse(wrapper, content_type=content_type)

    # Guardamos el tamaño del archivo en la variable response
    response['Content-Length'] = path.getsize(ruta)

    # Guardamos el tipo de archivo en la variable response
    response['Content-Type'] = content_type

        # response['Content-Disposition'] = 'attachment; filename={}'.format(nombre)

    # Devolvemos al navegador la variable response
    return response


def series(request):
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    else:
        titulo = "Bienvenido %s" %(request.user)

    archivos = {}
    archivos = Pelicula.objects.values_list('nombre', 'ruta', 'caratula').order_by('nombre')
    total = len(archivos)
    mitad = int(total / 2)

    if total % 2 != 0:
        mitad = mitad + 1
        archivos1 = archivos[0:mitad]
        archivos2 = archivos[mitad:]
    else:
        archivos1 = archivos[0:mitad]
        archivos2 = archivos[mitad:]

    context = {
        'archivos1': archivos1,
        'archivos2': archivos2,
        'titulo': titulo
    }

    return render(request, "peliculas.html", context)


def sinc_series(request):
    # Pelicula.objects.all().delete()
    caratulas = listdir('./static/images/caratulas/peliculas')

    for path, folders, elementos in walk("./static/media/videos"):  # ./media/videos/peliculas
        if len(elementos) > 0:
            for filename in elementos:
                path_rep = path.replace('\\', '/')
                ruta = path_rep + '/' + filename
                try:
                    pelicula = Pelicula.objects.get(nombre=filename)
                except:
                    pelicula = Pelicula()
                    pelicula.nombre = filename

                if filename in str(caratulas):
                    pelicula.caratula = 'images/caratulas/peliculas/' + filename
                pelicula.ruta = ruta
                pelicula.save()
                pelicula = Pelicula()

    return HttpResponseRedirect(reverse('peliculas'))


def descargars(request, ruta):
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    pelicula = Pelicula.objects.get(ruta='./' + ruta)
    nombre = pelicula.nombre.replace(' ', '_')
    fichero = open(ruta, 'rb')
    response = FileResponse(fichero)
    # wrapper = FileWrapper(fichero)
    content_type = mimetypes.guess_type(ruta)[0]
    # response = HttpResponse(wrapper, content_type=content_type)
    response['Content-Length'] = path.getsize(ruta)
    response['Content-Type'] = content_type
    # response['Content-Disposition'] = 'attachment; filename={}'.format(nombre)
    return response


def documentales(request):
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    else:
        titulo = "Bienvenido %s" %(request.user)

    archivos = {}
    archivos = Pelicula.objects.values_list('nombre', 'ruta', 'caratula').order_by('nombre')
    total = len(archivos)
    mitad = int(total / 2)

    if total % 2 != 0:
        mitad = mitad + 1
        archivos1 = archivos[0:mitad]
        archivos2 = archivos[mitad:]
    else:
        archivos1 = archivos[0:mitad]
        archivos2 = archivos[mitad:]

    context = {
        'archivos1': archivos1,
        'archivos2': archivos2,
        'titulo': titulo
    }

    return render(request, "peliculas.html", context)


def sinc_documentales(request):
    # Pelicula.objects.all().delete()
    caratulas = listdir('./static/images/caratulas/peliculas')

    for path, folders, elementos in walk("./static/media/videos"):  # ./media/videos/peliculas
        if len(elementos) > 0:
            for filename in elementos:
                path_rep = path.replace('\\', '/')
                ruta = path_rep + '/' + filename
                try:
                    pelicula = Pelicula.objects.get(nombre=filename)
                except:
                    pelicula = Pelicula()
                    pelicula.nombre = filename

                if filename in str(caratulas):
                    pelicula.caratula = 'images/caratulas/peliculas/' + filename
                pelicula.ruta = ruta
                pelicula.save()
                pelicula = Pelicula()

    return HttpResponseRedirect(reverse('peliculas'))


def descargard(request, ruta):
    user = request.user
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('inicio'))
    pelicula = Pelicula.objects.get(ruta='./' + ruta)
    nombre = pelicula.nombre.replace(' ', '_')
    fichero = open(ruta, 'rb')
    response = FileResponse(fichero)
    # wrapper = FileWrapper(fichero)
    content_type = mimetypes.guess_type(ruta)[0]
    # response = HttpResponse(wrapper, content_type=content_type)
    response['Content-Length'] = path.getsize(ruta)
    response['Content-Type'] = content_type
    # response['Content-Disposition'] = 'attachment; filename={}'.format(nombre)
    return response


def contact(request):
    titulo = "Hola, bienvenido al formulario de contacto"

    form = ContactForm(request.POST or None)

    context = {
        "wb_titulo": titulo,
        "wb_contform": form,
    }

    if form.is_valid():

        form_nombre = form.cleaned_data.get("nombre")
        form_email = form.cleaned_data.get("email")
        form_mensaje = form.cleaned_data.get("mensaje")
        asunto = "Form de Contacto"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "lezquerra@gmail.com"]
        mensaje_email = "%s: mensaje de prueba enviado por %s" %(form_nombre, form_email)
        send_mail(asunto,
                  mensaje_email,
                  email_from,
                  email_to,
                  fail_silently=False)

        context = {
            "wb_titulo": "Gracias %s" %(form_nombre),
        }

        if form_nombre == "SinNombre":
            context = {
                "wb_titulo": "Gracias %s!" %(form_email)
            }

        # Muestra en el CMD los datos obtenídos
        # print (form_nombre, form_email, form_mensaje)
        form.save()

    return render(request, "contacto.html", context)


def about(request):
    return render(request, "about.html", {})
