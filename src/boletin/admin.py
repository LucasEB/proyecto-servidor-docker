from django.contrib import admin

# Register your models here.
from .forms import RegModelForm
from .models import Registrado
from .forms import ContactForm
from .models import Contacto, Pelicula

class AdminRegistrado(admin.ModelAdmin):
    list_display = ["email", "nombre", "timestamp"]
    form = RegModelForm
    # list_display_links = ["nombre"]
    list_filter = ["email", "nombre"]
    list_editable = ["nombre"]
    search_fields = ["email", "nombre"]
    # class Meta:
    #     model = Registrado

admin.site.register(Registrado, AdminRegistrado)


class AdminContacto(admin.ModelAdmin):
    list_display = ["email", "nombre", "mensaje"]
    form = ContactForm
    list_filter = ["email", "nombre"]
    list_editable = ["nombre"]
    search_fields = ["email", "nombre"]

admin.site.register(Contacto, AdminContacto)


class AdminPeliculas(admin.ModelAdmin):
    list_display = ["nombre", "ruta"]

admin.site.register(Pelicula, AdminPeliculas)
