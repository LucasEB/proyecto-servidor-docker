"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from boletin import views
# from boletin.views inicio

urlpatterns = [
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^$', views.inicio, name='inicio'),
    url(r'^about/$', views.about, name='about'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^multimedia/$', views.multimedia, name='multimedia'),
    url(r'^galeria/$', views.galeria, name='galeria'),
    url(r'^galeria/(?P<titulo>.*)$', views.galeria, name='galeria'),
    url(r'^peliculas/$', views.peliculas, name='peliculas'),
    url(r'^descargarp/(?P<ruta>.*)$', views.descargarp, name='descargarp'),
    url(r'^sinc_peliculas/$', views.sinc_peliculas, name='sinc_peliculas'),
    url(r'^series/$', views.series, name='series'),
    url(r'^descargars/(?P<ruta>.*)$', views.descargars, name='descargars'),
    url(r'^sinc_series/$', views.sinc_series, name='sinc_series'),
    url(r'^documentales/$', views.documentales, name='documentales'),
    url(r'^descargard/(?P<ruta>.*)$', views.descargard, name='descargard'),
    url(r'^sinc_documentales/$', views.sinc_documentales, name='sinc_documentales')
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
